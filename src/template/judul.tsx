import { Animation } from "remotion-animation";
import "animate.css";
import { Sequence, useCurrentFrame} from "remotion";
import "./font.css";

export const Judul : React.FC<{
	titleText: string;
  dr: number;
  font: string;
  efek: string;
  animasi: string;
}> = ({titleText, dr, font,efek,animasi }) => {

  const bagi = dr/2;
  const x = bagi*30;

	return (
		<div style={{textAlign:"center", position:"relative", top:"30%", fontFamily:font,left:"5px"}} className={efek}>

		<Sequence from={0} > 
		<Animation duration={bagi} animateName={animasi}>
		<span style={{ fontSize: "90px",fontWeight: "bold"}}> {titleText} </span>
	</Animation>
	</Sequence>
	</div>
	);
};
