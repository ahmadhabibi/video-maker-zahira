import { Animation } from "remotion-animation";
import "animate.css";
import { Sequence, useCurrentFrame} from "remotion";
import "./font.css";

export const Gambar : React.FC<{
  dr: number;
  klip: string;
  animasi: string;
  url_gambar: string;
}> = ({dr,klip,animasi,url_gambar}) => {

  const bagi = dr/2;
  const x = bagi*30;

	return (
	<Sequence from={20}>
	<Animation duration={bagi} animateName={animasi}>
		<img src={url_gambar} style={{
			width:"1152px",
			height:"1080px",
			left:"768px",
			position: "relative",
			clipPath:klip,
		}} ></img>
		</Animation>
	</Sequence>
	);
};
