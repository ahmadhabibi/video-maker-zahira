import { Animation } from "remotion-animation";
import "animate.css";
import { Sequence, useCurrentFrame} from "remotion";
import { Judul } from "./template/judul";
import { Gambar } from "./template/gambar";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
  return images;
}
const images = importAll(require.context('./gambar', false, /\.(png|jpe?g|svg)$/));

export const MyComposition = ({titleText,bg,color,font,klip,efek,animasi,url_gambar}) => {

	return (
		<main style={{ width:"100%", height:"100%",backgroundColor : "#"+bg, color: "#"+color }}>
		<Gambar dr={5} klip={klip} animasi={animasi} url_gambar={url_gambar}></Gambar>
		<div style={{
			width: "40%",
			height:"100%",
			position: "fixed",
		}}>
		<Judul titleText={titleText} dr={5} font={font} efek={efek} animasi={animasi}></Judul>
		</div>
		</main>
	);
};
