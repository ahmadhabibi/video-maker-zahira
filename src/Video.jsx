import {Composition} from 'remotion';
import {HelloWorld} from './HelloWorld';
import {Logo} from './HelloWorld/Logo';
import {Subtitle} from './HelloWorld/Subtitle';
import {MyComposition} from './Dinamik';
import {Contoh} from './contoh';

export const RemotionVideo = () => {
	return (
		<>
			<Composition
				id="HelloWorld"
				component={HelloWorld}
				durationInFrames={150}
				fps={30}
				width={1920}
				height={1080}
				defaultProps={{
					titleText: 'Welcome to Remotion',
					titleColor: 'black',
				}}
			/>
			<Composition
				id="Logo"
				component={Logo}
				durationInFrames={200}
				fps={30}
				width={1920}
				height={1080}
			/>
			<Composition
				id="Title"
				component={Subtitle}
				durationInFrames={100}
				fps={30}
				width={1920}
				height={1080}
			/>
			<Composition
				id="Dinamik"
				component={MyComposition}
				durationInFrames={150}
				fps={30}
				width={1920}
				height={1080}
				defaultProps={{
					titleText: "ujiscobaa",
					bg: "000",
					color: "ffff",
					font: "Roboto slab",
					klip: "polygon(0 0, 100% 0, 100% 100%, 0% 100%)",
					efek: "font-effect-3d-float",
					animasi:"slideInUp",
					url_gambar: "https://i.picsum.photos/id/866/1000/1000.jpg?hmac=6eKrL6QHaG5cwQWxX70J0v_KiidLGDTcUMW78JZT1yQ",
				}}
			/>

			<Composition
				id="contoh"
				component={Contoh}
				durationInFrames={300}
				fps={30}
				width={1920}
				height={1080}
			/>
		</>
	);
};
