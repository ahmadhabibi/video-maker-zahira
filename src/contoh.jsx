import { Sequence, useCurrentFrame} from "remotion";

export const Contoh = () => {

	return (
		<contohvideo>

		<div style={{width: '1920px', height: '1080px', background: 'white'}} />
		
		<Sequence from={0} durationInFrames={60}>
		<div style={{width: '1920px', height: '1080px', background: 'blue'}} />
		</Sequence>

		<Sequence from={60} durationInFrames={60}>
		<div style={{width: '1920px', height: '1080px', background: 'red', position: 'relative'}} />
		</Sequence>

		<Sequence from={120} durationInFrames={60}>
		<div style={{width: '1920px', height: '1080px', background: 'green', position: 'relative'}} />
		</Sequence>

		<Sequence from={180} durationInFrames={60}>
		<div style={{width: '1920px', height: '1080px', background: 'pink', position: 'relative'}} />
		</Sequence>

		<Sequence from={240} durationInFrames={60}>
		<div style={{width: '1920px', height: '1080px', background: 'yellow', position: 'relative'}} />
		</Sequence>
		</contohvideo>
	);
};
