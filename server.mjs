/**
 * This is an example of a server that returns dynamic video.
 * Run `npm run server` to try it out!
 * If you don't want to render videos on a server, you can safely
 * delete this file.
 */

import {bundle} from '@remotion/bundler';
import {
	getCompositions,
	renderFrames,
	stitchFramesToVideo,
} from '@remotion/renderer';
import express from 'express';
import fs from 'fs';
import os from 'os';
import path from 'path';
import { Random } from "random-js";

const app = express();
const port = process.env.PORT || 8000;
const ip = "http://194.233.90.78";
const compositionId = 'Dinamik';

const font = ["Open Sans","Merriweather","Arvo","Roboto Slab","Ubuntu","Montserrat","Raleway","PT Sans","Josefin Sans","Catamaran"];
const clip = ["polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%)","polygon(50% 0%, 100% 38%, 82% 100%, 18% 100%, 0% 38%)","inset(5% 20% 15% 10%)","circle(50% at 50% 50%)","polygon(25% 0%, 100% 0%, 100% 100%, 25% 100%, 0% 50%)","polygon(20% 0%, 80% 0%, 100% 100%, 0% 100%)","polygon(40% 0%, 40% 20%, 100% 20%, 100% 80%, 40% 80%, 40% 100%, 0% 50%)","polygon(50% 0%, 90% 20%, 100% 60%, 75% 100%, 25% 100%, 0% 60%, 10% 20%)","polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)","polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%)","polygon(25% 0%, 100% 0%, 75% 100%, 0% 100%)","polygon(50% 0%, 80% 10%, 100% 35%, 100% 70%, 80% 90%, 50% 100%, 20% 90%, 0% 70%, 0% 35%, 20% 10%)","polygon(100% 0%, 75% 50%, 100% 100%, 25% 100%, 0% 50%, 25% 0%)","polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%)","polygon(50% 0%, 83% 12%, 100% 43%, 94% 78%, 68% 100%, 32% 100%, 6% 78%, 0% 43%, 17% 12%)","polygon(20% 0%, 80% 0%, 100% 20%, 100% 80%, 80% 100%, 20% 100%, 0% 80%, 0% 20%)","polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%)"]
const efek = ["font-effect-3d","font-effect-outline","font-effect-3d-float","font-effect-shadow-multiple"]
const warna = [["FF80ED","80FF92"],["065535","550626"],["133337","371713"],["FFC0CB","C0FFF4"],["FFE4E1","E1FCFF"],["008080","800000"],["FF0000","00FFFF"],["E6E6FA","FAFAE6"],["FFD700","0028FF"],["FFA500","005AFF"],["00FFFF","FF0000"],["FF7373","73FFFF"],["40E0D0","E04050"],["0000FF","FFFF00"],["D3FFCE","FACEFF"],["F0F8FF","FFF7F0"],["B0E0E6","E6B6B0"],["C6E2FF","FFE3C6"],["FAEBD7","D7E6FA"],["BADA55","7555DA"],["003366","663300"],["FFB6C1","B6FFF4"],["FA8072","72ECFA"],["FFFF00","0000FF"],["800000","008080"],["7FFFD4","FF7FAA"],["800080","008000"],["C39797","97C3C3"],["00FF00","FF00FF"],["F08080","80F0F0"],["FFF68F","8F98FF"],["20B2AA","B22028"],["FFC3A0","A0DCFF"],["66CDAA","CD6689"],["FF6666","66FFFF"],["FFDAB9","B9DEFF"],["C0D6E4","E4CEC0"],["FF00FF","00FF00"],["FF7F50","50D0FF"],["AFEEEE","EEAFAF"],["468499","995B46"],["CBBEB5","B5C2CB"],["008000","800080"],["00CED1","D10300"],["B4EEB4","EEB4EE"],["F6546A","54F6E0"],["B6FCD5","FCB6DD"],["660066","006600"],["0E2F44","44230E"],["DAA520","2055DA"],["990000","009999"],["000080","808000"],["088DA5","A52008"],["6897BB","BB8C68"],["8B0000","008B8B"],["F5F5DC","DCDCF5"],["FFFF66","6666FF"],["81D8D0","D88189"],["8A2BE2","83E22B"],["0A75AD","AD420A"],["2ACAEA","EA4A2A"],["CCFF00","3300FF"],["FF4040","40FFFF"],["66CCCC","CC6666"],["420420","044226"],["A0DB8E","C98EDB"],["00FF7F","FF0080"],["FF1493","14FF80"],["794044","407975"],["CC0000","00CCCC"],["3399FF","FF9933"]]
const anim = ["fadeInTopRight","backInDown","slideInDown","zoomInDown","rotateInDownLeft","rotateIn","fadeInRightBig","rotateInUpRight","bounceIn","bounceInDown","bounceInLeft","backInUp","rollIn","jackInTheBox","slideInRight","fadeInTopLeft","lightSpeedInLeft","fadeInBottomLeft","zoomInUp","lightSpeedInRight","fadeInBottomRight","bounceInUp","backInLeft","fadeInUpBig","fadeInDown","backInRight","zoomInLeft","fadeInUp","fadeIn","zoomIn","flipInX","slideInLeft","flipInY","fadeInLeft","fadeInRight","rotateInUpLeft","zoomInRight","fadeInLeftBig","bounceInRight","slideInUp","fadeInDownBig","rotateInDownRight"]

let uploadDir = path.join(process.cwd(), 'upload');

//setting random
const randomNumber = new Random(); // uses the nativeMath engine

Array.prototype.random = function () {
  return this[Math.floor(randomNumber.integer(0, this.length))];
}

const cache = new Map();

app.get('/', async (req,res) => {
	res.json({
		status: "running",
		url_render: req.hostname+":"+port+"/render?judul=judulnya&urlgambar=urlgambarnya",
		url_video: req.hostname+":"+port+"/video?judul=judulnya&urlgambar=urlgambarnya",
		url_thumbnail: req.hostname+":"+port+"/thumbnail?judul=judulnya&urlgambar=urlgambarnya",
	});

});

app.get('/cek', async (req,res) => {
	res.json({
		status: warna.random()
	});

});

app.get('/render', async (req, res) => {
	var wrn = warna.random()
	const keterangan = {
		bg : wrn[0],
		color : wrn[1],
		font : font.random(),
		klip : clip.random(),
		efek : efek.random(),
		animasi : anim.random(),
		};
	keterangan.titleText = req.query.judul;

	if (req.query.url_gambar){
		keterangan.url_gambar = req.query.url_gambar;
	}


	try {
		if (cache.get(JSON.stringify(req.query))) {
			var queryString = Object.keys(req.query).map(key => key + '=' + req.query[key]).join('&');
			res.json({
			status: "sukses",
			video: req.hostname+":"+port+"/video?"+queryString,
			thumbnail: req.hostname+":"+port+"/thumbnail?"+queryString,
		});
			return;
		}
		const bundled = await bundle(path.join(process.cwd(), './src/index.jsx'));
		const comps = await getCompositions(bundled);
		const video = comps.find((c) => c.id === compositionId);
		if (!video) {
			throw new Error(`No video called ${compositionId}`);
		}
		// res.set('content-type', 'video/mp4');

		const tmpDir = await fs.promises.mkdtemp(
			path.join(process.cwd(),'render', 'remotion-')
		);
		const {assetsInfo} = await renderFrames({
			config: video,
			webpackBundle: bundled,
			onStart: () => console.log('Rendering frames...'),
			onFrameUpdate: (f) => {
				if (f % 10 === 0) {
					console.log(`Rendered frame ${f}`);
				}
			},
			parallelism: null,
			outputDir: tmpDir,
			inputProps: keterangan,
			compositionId,
			imageFormat: 'jpeg',
		});

		const finalOutput = path.join(tmpDir, 'out.mp4');
		await stitchFramesToVideo({
			dir: tmpDir,
			force: true,
			fps: video.fps,
			height: video.height,
			width: video.width,
			outputLocation: finalOutput,
			imageFormat: 'jpeg',
			assetsInfo,
		});
		cache.set(JSON.stringify(req.query), tmpDir);
		var queryString = Object.keys(req.query).map(key => key + '=' + req.query[key]).join('&');
		res.json({
			status: "sukses",
			video: req.hostname+":"+port+"/video?"+queryString,
			thumbnail: req.hostname+":"+port+"/thumbnail?"+queryString,
		});
		// sendFile(finalOutput);
		console.log('Video rendered and sent!');
	} catch (err) {
		console.error(err);
		res.json({
			error: err,
		});
	}
});

app.get('/video', async (req, res) => {

	const sendFile = (file) => {
		fs.createReadStream(file)
			.pipe(res)
			.on('close', () => {
				res.end();
			});
	};

	const folder = cache.get(JSON.stringify(req.query));
	if(folder) {
		res.set('content-type', 'video/mp4');
		sendFile(path.join(folder,"out.mp4"));
	} else {
		res.json({
	        status : "error",
	        keterangan : "Video belum dirender"
	        });
	}

});

app.get('/thumbnail', async (req, res) => {

	const sendFile = (file) => {
		fs.createReadStream(file)
			.pipe(res)
			.on('close', () => {
				res.end();
			});
	};

	const folder = cache.get(JSON.stringify(req.query));
	if (folder){
		fs.readdir(folder, function (err, files) {
	    const gambar = files.filter(el => path.extname(el) === '.jpeg').slice(-1).pop();
	    res.set('content-type', 'image/jpeg');
	    sendFile(path.join(cache.get(JSON.stringify(req.query)),gambar));
	});
	} else {
		res.json({
	        	status : "error",
	        	keterangan : "Video belum dirender"
	        });
	}

});


// Handle file upload

app.get("/up",(req,res)=>{
	let indexFile = path.join(process.cwd(),'src', 'upload.html');
  fs.readFile(indexFile, (err, content) => {
    if(err){
      res.writeHead(404, {'Content-Type': 'text'});
      res.write('File Not Found!');
      res.end();
    }else{
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(content);
      res.end();
    }
  });
});

app.post('/upload/:url', (req, res) => {
  console.log('saving uploaded file');
  let fileName = path.basename(req.params.url);
  let file = path.join(uploadDir, fileName)
  req.pipe(fs.createWriteStream(file));
  req.on('end', () => {
    res.json({
    	status: "sukses",
    	filename: fileName,
    });
    res.end();
  })
});

app.get('/list', (req, res) => {
  
  fs.readdir(uploadDir, (err, files) => {
    if(err){
      console.log(err);
      res.writeHead(400, {'Content-Type': 'application/json'});
      res.write(JSON.stringify(err.message));
      res.end();
    }else{
      res.writeHead(200, {'Content-Type': 'application/json'});
      res.write(JSON.stringify(files));
      res.end();
    }
  })
});

app.get('/download/:url', (req, res) => {
  let file = path.join(uploadDir, req.params.url);
  fs.readFile(file, (err, content) => {
    if(err){
      res.writeHead(404, {'Content-Type': 'text'});
      res.write('File Not Found! '+file);
      res.end();
    }else{
      res.writeHead(200, {'Content-Type': 'application/octet-stream'});
      res.write(content);
      res.end();
    }
  })
});

app.listen(port);

console.log(
	[
		`The server has started on ${ip}:${port}! `,
		'You can render a video by passing props as URL parameters.',
		'',
		'If you are running Hello World, try this:',
		'',
		`${ip}:${port}?judul=contoh text`,
		'',
	].join('\n')
);
